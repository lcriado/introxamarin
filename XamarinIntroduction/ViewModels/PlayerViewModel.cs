﻿using System;
using XamarinIntroduction.Models;

namespace XamarinIntroduction.ViewModels
{
    public class PlayerViewModel : BaseViewModel
    {
        public string   playerName { get; set; }
        public int      number { get; set; }
        public string   position { get; set; }
        public bool     active { get; set; }
        public string   imageUrl { get; set; }


        public PlayerViewModel() {}

        public PlayerViewModel(Player player) {
            playerName  = player.playerName;
            number      = player.number;
            position    = player.position;
            active      = player.active;
            imageUrl    = player.imageUrl;
        }

    }
}
