﻿using System.Collections.ObjectModel;
using XamarinIntroduction.Models;
using System.Windows.Input;
using Xamarin.Forms;
using System.Threading.Tasks;
using Realms;
using System;
using System.Net.Http;
using Newtonsoft.Json;
using System.Diagnostics;

namespace XamarinIntroduction.ViewModels
{
    public class PlayersListPageViewModel : BaseViewModel
    {
        
        public ObservableCollection<Player> Players { get; private set; }
        = new ObservableCollection<Player>();

        readonly Realm context = Realm.GetInstance();

        public  Player SelectedPlayer { get; set; }

        public INavigation Navigation { get; set; }
        public ICommand AddPlayerCommand { get; private set; }


        //----------------------------------------------------------------------
        public PlayersListPageViewModel(INavigation navigation)
        {
            Navigation = navigation;

            AddPlayerCommand = new Command(execute: async () => await AddPlayer());

            PrepareDatabase();

            var players = context.All<Player>();
            Players = new ObservableCollection<Player>(players);
        }

        //----------------------------------------------------------------------
        async Task AddPlayer()
        {
            var viewModel = new PlayerDetailPageViewModel(new Player(), Navigation);

            viewModel.PlayerAdded += (source, player) =>
            {
                // Add player to DB
                context.Write(() => { context.Add(player); });

                // Add player to view list
                Players.Add(player);
            };

            await Navigation.PushAsync(new PlayerDetailPage(viewModel));
        }

        //----------------------------------------------------------------------
        async public void ViewSelectedPlayer(Player player)
        {
            var viewModel = new PlayerDetailPageViewModel(player, Navigation);
            await Navigation.PushAsync(new PlayerDetailPage(viewModel));
        }

        //----------------------------------------------------------------------
        async void PrepareDatabase()
        {

            // To make it work, from terminal go to ./Resources and write:
            // npm install -g json-server
            // json-server --watch players.json

            var httpClient = new HttpClient();
            var response = await httpClient.GetAsync("http://localhost:3000/players");

            var players = new ObservableCollection<Player>();
            if (response.IsSuccessStatusCode)
            {
                var jsonAsString = await response.Content.ReadAsStringAsync();
                players = JsonConvert.DeserializeObject<ObservableCollection<Player>>(jsonAsString);
                Debug.WriteLine(jsonAsString);
            }


            context.Write(() =>
            {
                context.RemoveAll<Player>();

                foreach(var player in players) 
                    context.Add(player, update: true);
            });
        }
    }
}
