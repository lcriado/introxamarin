﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using XamarinIntroduction.Models;
using Xamarin.Forms;
using System.Diagnostics;
using Plugin.Connectivity;

namespace XamarinIntroduction.ViewModels
{
    public class PlayerDetailPageViewModel
    {
        public event EventHandler<Player> PlayerAdded;

        public Player Player { get; private set; }

        public ICommand SaveCommand { get; private set; }
        public INavigation Navigation { get; set; }


        public PlayerDetailPageViewModel(Player player, INavigation navigation)
        {
            Navigation = navigation;
            SaveCommand = new Command(execute: async () => await Save());

            if(player.imageUrl == null) {
                player.imageUrl = "https://media.licdn.com/dms/image/C5603AQHSbShADsTn4w/profile-displayphoto-shrink_200_200/0?e=1528570800&v=beta&t=l3ACW_IM0gdl2EojX5b-Bxid6jzMx5-TJRIfKYzTBkM";
            } 

            Player = new Player
            {
                playerName = player.playerName,
                number = player.number,
                position = player.position,
                active = player.active,
                imageUrl = player.imageUrl,
            };
        }

        async Task Save()
        {
            var isConnected = CrossConnectivity.Current.IsConnected;
            Debug.WriteLine(isConnected);

            if (String.IsNullOrWhiteSpace(Player.playerName) && !isConnected) // Only save if is connected to wifi
                return;
            

            PlayerAdded?.Invoke(this, Player);

            await Navigation.PopAsync();
        }
    }
}
