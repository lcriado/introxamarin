﻿using Xamarin.Forms;
using XamarinIntroduction.ViewModels;

namespace XamarinIntroduction
{
    public partial class PlayerDetailPage : ContentPage
    {
         public PlayerDetailPage(PlayerDetailPageViewModel viewModel)
        {
            InitializeComponent();
            BindingContext = viewModel;
        }
    }
}
