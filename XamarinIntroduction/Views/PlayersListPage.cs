﻿using Xamarin.Forms;
using XamarinIntroduction.Models;
using XamarinIntroduction.ViewModels;
using System;

namespace XamarinIntroduction
{
    public partial class PlayersListPage : ContentPage
    {
        public PlayersListPageViewModel ViewModel
        {
            get { return BindingContext as PlayersListPageViewModel; }
            set { BindingContext = value; }
        }

        public PlayersListPage()
        {
            ViewModel = new PlayersListPageViewModel(Navigation);
            InitializeComponent();
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var player = (Player)e.Item;
            ViewModel.ViewSelectedPlayer(player);
        }
    }
}
