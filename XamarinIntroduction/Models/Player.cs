﻿using System;
using Realms;

namespace XamarinIntroduction.Models
{
    public class Player : RealmObject
    {
        public string   playerName { get; set; }
        public int      number { get; set; }
        public string   position { get; set; }
        public bool     active { get; set; }
        public string   imageUrl { get; set; }
    }
}
